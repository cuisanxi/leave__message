package team.chuangshu.lm.KeyUtileTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*@RunWith(SpringRunner.class)
@SpringBootTest*/
public class test {
    public static String generateRandomStr(int len) {
         //字符源，可以根据需要增减
        String generateSource = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
        String rtnStr = "";
        for (int i = 0; i < len; i++) {
            //循环随机获得当次字符，并移走选出的字符
            String nowStr = String.valueOf(generateSource.charAt((int) Math.floor(Math.random() * generateSource.length())));
            rtnStr += nowStr;
            generateSource = generateSource.replaceAll(nowStr, "");
        }
        String timeNum = String.valueOf(System.currentTimeMillis());
        return rtnStr+timeNum.substring(timeNum.length()-4,timeNum.length());
}

    @Test
    public  void One() {
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i <10000000 ; i++) {
            String s = generateRandomStr(4);
            System.out.println(s);
            if (result.contains(s)) {
                System.out.println("出错"+result.size());
                break;
            }
            result.add(s);
        }
    }
}
