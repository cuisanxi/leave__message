package team.chuangshu.lm.PDFserviceTest;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.test.context.junit4.SpringRunner;
import sun.rmi.runtime.Log;
import team.chuangshu.lm.dto.MessageDTO;
import team.chuangshu.lm.entity.*;
import team.chuangshu.lm.service.*;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PDFTest {

    private  static  Map o  = new HashMap();

    @Test
    public  void test() {
        o.put("name","第一个测试saddfa sfas fasdf asdf saddf Sasd asdf asd fasdf asdf\n asdf ads fadsfg dsfg sdf sdf  ");
        // 模板路径
        String templatePath = "C:\\Users\\zhang\\Desktop\\tempalte.pdf";
        // 生成的新文件路径
        String newPDFPath = "C:\\Users\\zhang\\Desktop\\test.pdf";

        PdfReader reader;
        FileOutputStream out;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        try {
            //BaseFont bf = BaseFont.createFont("c://windows//fonts//simsun.ttc,1" , BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont bf =BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
            Font FontChinese = new Font(bf, 5, Font.NORMAL);
            out = new FileOutputStream(newPDFPath);// 输出流
            reader = new PdfReader(templatePath);// 读取pdf模板
            bos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, bos);
            AcroFields form = stamper.getAcroFields();
            //文字类的内容处理
            Map<String,String> datemap = o;
            form.addSubstitutionFont(bf);
            for(String key : datemap.keySet()){
                String value = datemap.get(key);
                form.setField(key,value);
            }

            stamper.setFormFlattening(true);// 如果为false，生成的PDF文件可以编辑，如果为true，生成的PDF文件不可以编辑
            stamper.close();
            Document doc = new Document();
            Font font = new Font(bf, 32);
            PdfCopy copy = new PdfCopy(doc, out);
            doc.open();
            PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), 1);
            copy.addPage(importPage);
            doc.close();

        } catch (IOException e) {
            System.out.println(e);
        } catch (DocumentException e) {
            System.out.println(e);
        }
    }
    @Autowired
    private MessageService messageService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private ClazzService clazzService;

    @Autowired
    private FacultyService facultyService;

    @Test
    public  void test1() throws Exception {

        Message message = messageService.get("BMDH3879");
        MessageDTO messageDTO = new MessageDTO(message);

        //补充老师信息
        Teacher teacher = new Teacher();
        teacher.setUserId(message.getTeacherId());
        teacher = teacherService.findByParams(teacher).get(0);
        messageDTO.setTeacherName(teacher.getName());
        messageDTO.setTeacherPhone(teacher.getPhone());
        //补充班级信息
        Student student = new Student();
        student.setUserId(message.getStudentId());
        Student student1 = new Student();
        student1.setUserId(message.getStudentId());
        List<Student> studentList = studentService.findByParams(student1);
        System.out.println(student);
        Clazz clazz = clazzService.get(studentList.get(0).getClazzId());
        messageDTO.setClassName(clazz.getClazzName());
        //补充学院信息
        Faculty faculty = facultyService.get(clazz.getParentId());
        messageDTO.setFacultyName(faculty.getFacultyName());
        messageDTO.setFacultyTeacher(faculty.getName());
        //对象转换成map
        Map<String, String> map = new HashMap();
       /* if (messageDTO != null) {
            BeanMap beanMap = BeanMap.create(messageDTO);
            for (Object key : beanMap.keySet()) {
                if (beanMap.get(key) instanceof java.util.Date) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    map.put(key+"",sdf.format(beanMap.get(key)) );
                }else {
                    System.out.println(key);
                    System.out.println(beanMap.get(key).getClass());
                    //map.put(key+"", beanMap.get(key).toString());
                }
            }
        }*/

        if(messageDTO  != null){
            BeanInfo beanInfo = Introspector.getBeanInfo(messageDTO .getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (!key.equals("class")) {
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(messageDTO);
                    System.out.println(key);
                    System.out.println(value);
                    if (value == null) {
                        value = "";
                    }
                    if (value instanceof java.util.Date) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        value = sdf.format(value);
                    }
                    map.put(key, value.toString());
                }
            }
        }

        if (messageDTO.getIsLongtime() == 0) {
            map.remove("facultyTeacher");
        }

        String templatePath = "F:\\leaveMessage\\tamplate\\template.pdf";
        // 生成的新文件路径
        String newPDFPath = "F:\\leaveMessage\\tamplate\\test.pdf";

        PdfReader reader;
        FileOutputStream out;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        BaseFont bf = BaseFont.createFont("c://windows//fonts//simsun.ttc,1" , BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //BaseFont bf =BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
       // BaseFont bf = BaseFont.createFont();
        Font FontChinese = new Font(bf, 5, Font.NORMAL);
        out = new FileOutputStream(newPDFPath);// 输出流
        reader = new PdfReader(templatePath);// 读取pdf模板
        bos = new ByteArrayOutputStream();
        stamper = new PdfStamper(reader, bos);
        AcroFields form = stamper.getAcroFields();
        //文字类的内容处理
        Map<String,String> datemap = map;
        form.addSubstitutionFont(bf);
        for(String key : datemap.keySet()){
            String value = datemap.get(key);
            form.setField(key,value);
        }

        stamper.setFormFlattening(true);// 如果为false，生成的PDF文件可以编辑，如果为true，生成的PDF文件不可以编辑
        stamper.close();
        Document doc = new Document();
        Font font = new Font(bf, 32);
        PdfCopy copy = new PdfCopy(doc, out);
        doc.open();
        PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), 1);
        copy.addPage(importPage);
        doc.close();
    }
}
