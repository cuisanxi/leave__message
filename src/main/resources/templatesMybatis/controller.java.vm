package ${package.Controller};


#if(${superControllerClassPackage})
import ${superControllerClassPackage};

#end
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import #parse("templatesMybatis/config.vm")base.ResultUtil;
import #parse("templatesMybatis/config.vm")base.dto.ResultDTO;
import #parse("templatesMybatis/config.vm")base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};

import java.util.List;


/**
 *
 * @author ${author}
 * @since ${date}
 */
#if(${restControllerStyle})
@RestController
#else
@RestController
#end
@RequestMapping("#if(${package.ModuleName})/${package.ModuleName}#end/#if(${controllerMappingHyphenStyle})${controllerMappingHyphen}#else${table.entityPath}#end")
#if(${superControllerClass})
public class ${table.controllerName} extends ${superControllerClass} {
#else
public class ${table.controllerName} {
#end

    @Autowired
    public ${table.serviceName} ${table.entityPath}Service;

    @PostMapping("/insert${entity}")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(${entity} ${table.entityPath}) throws Exception {
        ${table.entityPath}Service.save(${table.entityPath});
        return ResultUtil.Success(${table.entityPath});
    }

    @PutMapping("/update${entity}ById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(${entity} ${table.entityPath}) throws Exception {
        ${table.entityPath}Service.update(${table.entityPath});
        return ResultUtil.Success(${table.entityPath}Service.get(${table.entityPath}.getId()));
    }

    @DeleteMapping("/delete${entity}ById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        ${entity} old${entity} = ${table.entityPath}Service.get(id);
        ${table.entityPath}Service.deleteById((long) id);
        return ResultUtil.Success(old${entity});
    }

    @GetMapping("/get${entity}Pages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<${entity}> selectEntityPage(${entity} ${table.entityPath}, int pageNum, int pageSize)throws Exception{
        return ${table.entityPath}Service.findPageData(${table.entityPath}, pageNum, pageSize);
    }

    @GetMapping("/get${entity}List")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(${entity} ${table.entityPath})throws Exception{
        return ResultUtil.Success(${table.entityPath}Service.findByParams(${table.entityPath}));
    }


}
