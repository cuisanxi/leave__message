--liquibase formatted sql
--changeset zhang:init

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for clazz
-- ----------------------------
DROP TABLE IF EXISTS `clazz`;
CREATE TABLE `clazz`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '班级id,自增',
  `clazz_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '学院班级名称',
  `parent_id` int(10) NOT NULL COMMENT '父母id，学院id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_swedish_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for faculty
-- ----------------------------
DROP TABLE IF EXISTS `faculty`;
CREATE TABLE `faculty`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '学院表主键，自增',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '联系方式',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '姓名',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '学院负责人（副书记）用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `message_id` varchar(8) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '假条id，8位数字与字母混合',
  `student_id` varchar(12) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '学生用户id',
  `begin_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '假条开始时间',
  `end_time` timestamp(6) NOT NULL COMMENT '假条结束时间',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '联系方式',
  `teacher_id` varchar(12) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '负责老师用户id',
  `file_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '申请资料存放路径',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '请假理由',
  `creat_time` timestamp(6) NOT NULL COMMENT '申请时间',
  `is_longtime` int(1) NOT NULL COMMENT '长假标识，0标识短假，1标识长假',
  `level_one_sign` int(255) NOT NULL COMMENT '一级审查标识，默认0，同意1，拒绝2',
  `level_one_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NULL DEFAULT NULL COMMENT '一级审查意见',
  `level_one_time` timestamp(6) NULL DEFAULT NULL COMMENT '一级审查意见时间',
  `level_two_sign` int(255) NOT NULL COMMENT '二级审查标识，默认0，同意1，拒绝2',
  `level_two_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NULL DEFAULT NULL COMMENT '二级审查意见',
  `level_two_time` timestamp(6) NULL DEFAULT NULL COMMENT '二级审查意见时间',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '状态码，默认0，表示请假，1，表示申请通过，2表示申请被拒绝，3，待销假，4，已申请销假，5，同意销假',
  `is_reportback` int(255) NULL DEFAULT NULL COMMENT '销假标识，默认0，1，申请销假',
  `reportback_submit_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NULL DEFAULT NULL COMMENT '学生在销假时提交的给负责人的内容',
  PRIMARY KEY (`message_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role_url_connect
-- ----------------------------
DROP TABLE IF EXISTS `role_url_connect`;
CREATE TABLE `role_url_connect`  (
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '角色名称',
  `url_id` int(10) NOT NULL COMMENT 'url id',
  PRIMARY KEY (`role_name`, `url_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '学生表主键，自增',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '姓名',
  `class_id` int(10) NOT NULL COMMENT '所在班级在class表中id',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '学生在user表中id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '老师用户id',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '联系方式',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '姓名',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '教师在user表中id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teacher_class_connect
-- ----------------------------
DROP TABLE IF EXISTS `teacher_class_connect`;
CREATE TABLE `teacher_class_connect`  (
  `teacher_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '老师的user_id',
  `class_id` int(10) NOT NULL COMMENT '班级id，班级id不能为0',
  PRIMARY KEY (`teacher_id`, `class_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for url
-- ----------------------------
DROP TABLE IF EXISTS `url`;
CREATE TABLE `url`  (
  `url_id` int(10) NOT NULL COMMENT 'url id',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT 'url 路径',
  PRIMARY KEY (`url_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '用户id',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '登录密码',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

--liquibase formatted sql
--changeset zhang：增加学院表，学院名字
alter table faculty add faculty_name varchar (255) not null COMMENT "学院名字";
alter table message add  clazz_num varchar (255) not null COMMENT "请假节数";

--liquibase formatted sql
--changeset zhang：修改 teacher_class_connect表teacher_id数据类型
alter table teacher_class_connect modify column teacher_id int(10) COMMENT "teacher表的id";

--liquibase formatted sql
--changeset zhang：修改 student表class_id列名称为Clazz_id,增加message表学生名字字段
alter table student change column class_id clazz_id INT ( 10 ) not null COMMENT "班级id";
ALTER TABLE message ADD student_name VARCHAR ( 10 ) NOT NULL COMMENT "学生名字";