package team.chuangshu.lm.base;

import java.util.Random;

/**
 * @author Zhang
 * @date 2018-02-22 22:31
 **/
public class KeyUtil {

    //8位数字字母字符串
    public static synchronized String getUniqueKey(){
        //字符源，可以根据需要增减
        String generateSource = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
        String rtnStr = "";
        for (int i = 0; i < 4; i++) {
            //循环随机获得当次字符，并移走选出的字符
            String nowStr = String.valueOf(generateSource.charAt((int) Math.floor(Math.random() * generateSource.length())));
            rtnStr += nowStr;
            generateSource = generateSource.replaceAll(nowStr, "");
        }
        String timeNum = String.valueOf(System.currentTimeMillis());
        return rtnStr+timeNum.substring(timeNum.length()-4,timeNum.length());
    }
}
