package team.chuangshu.lm.dto;

import lombok.Data;

/**
 * @auther zhang
 * @ TO DO
 * @creatTime 2019/5/8 20:49
 **/

@Data
public class UserDTO {

    private String userId;

    private String role;

    private String userName;

    private String facultyName;

    private String clazzName;
}
