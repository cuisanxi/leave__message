package team.chuangshu.lm.dto;


import lombok.Data;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.springframework.format.annotation.DateTimeFormat;
import team.chuangshu.lm.entity.Message;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@Data
public class MessageDTO {

    /**
     * 假条id，8位数字与字母混合
     */

    private String messageId;

    /**
     * 学院名字
     */
    private String facultyName;

    /**
     * 班级名字
     */
    private String className;

    /**
     * 学生用户id
     */
    private String studentId;

    /**
     * 学生用户id
     */
    private String studentName;

    /**
     * 假条开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date beginTime;

    /**
     * 假条结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date endTime;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 负责老师用户名字
     */
    private String teacherName;

    /**
     * 负责老师联系方式
     */
    private String teacherPhone;

    /**
     * 申请资料存放路径
     */
    private String fileUrl;

    /**
     * 请假理由
     */
    private String reason;

    /**
     * 请假节数
     */
    private String clazzNum;

    /**

    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date creatTime;

    /**
     * 长假标识，0标识短假，1标识长假
     */
    private Integer isLongtime;

    /**
     * 一级审查标识，默认0，同意1，拒绝2
     */
    private Integer levelOneSign;

    /**
     * 一级审查意见
     */
    private String levelOneComment;

    /**
     * 一级审查意见时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date levelOneTime;

    /**
     * 二级审查标识，默认0，同意1，拒绝2
     */
    private Integer levelTwoSign;

    /**
     * 学院负责人名字
     */
    private String facultyTeacher;

    /**
     * 二级审查意见
     */
    private String levelTwoComment;

    /**
     * 二级审查意见时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date levelTwoTime;

    /**
     * 状态码，默认0，表示请假，1，表示申请通过，2表示申请被拒绝，3，待销假，4，已申请销假，5，同意销假
     */
    private String status;

    /**
     * 销假标识，默认0，1，申请销假
     */
    private Integer isReportback;

    /**
     * 学生在销假时提交的给负责人的内容
     */
    private String reportbackSubmitComment;

    public MessageDTO(){}

    public MessageDTO(Message message) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        PropertyUtils.copyProperties(this, message);
    }
}
