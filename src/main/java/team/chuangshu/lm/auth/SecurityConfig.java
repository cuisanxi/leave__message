package team.chuangshu.lm.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @auther zhang
 * @ TO DO
 * @creatTime 2019/5/9 21:14
 **/
@Configurable
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;
    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new MyPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .loginPage("/views/zqu/login.html")
                .loginProcessingUrl("/login")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and()
                .authorizeRequests()
                /*.antMatchers(
                        "/CheckReport.html/**", "/managerIndex.html/**", "/MovieList.html/**", "MovieDetail.html/**",
                        "/TicketInformation.html/**", "/CheckReport.html/**", "/changeCinemaName.html/**", "/TicketList.html/**")*/
                .antMatchers("html/index.html")
                .hasRole("admin")
                .antMatchers("html/index.html")
                .authenticated()
                .anyRequest()
                .permitAll()
                .and()
                .cors()
                .and()
                .headers().frameOptions().disable()
                .and()
                .csrf().disable();
    }
}
