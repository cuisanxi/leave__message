package team.chuangshu.lm.auth;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @auther zhang
 * @ TO DO
 * @creatTime 2019/5/9 21:53
 **/
public class MyPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        //不做任何加密处理
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        //charSequence是前端传过来的密码，s是数据库中查到的密码
        if (charSequence.toString().equals(s)) {
            return true;
        }
        return false;
    }
}