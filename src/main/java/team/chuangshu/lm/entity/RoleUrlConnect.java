package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class RoleUrlConnect {



    /**
     * 角色名称
     */
    
    @Id
    @Column(name = "role_name")
                private String roleName;

    /**
     * url id
     */
    @Column(name = "url_id")
        private Integer urlId;


}


