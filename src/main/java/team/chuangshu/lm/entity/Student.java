package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Student {

    /**
     * 学生表主键，自增
     */

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;


    /**
     * 学生在user表中id
     */
    @Column(name = "user_id")
        private String userId;

    /**
     * 姓名
     */
    @Column(name = "name")
        private String name;

    /**
     * 所在班级在class表中id
     */
    @Column(name = "clazz_id")
        private Integer clazzId;



}


