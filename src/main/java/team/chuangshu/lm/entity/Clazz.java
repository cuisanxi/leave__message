package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Clazz {



    /**
     * 班级id
     */
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
                private Integer id;

    /**
     * 学院班级名称
     */
    @Column(name = "clazz_name")
        private String clazzName;

    /**
     * 父母id，学院id
     */
    @Column(name = "parent_id")
        private Integer parentId;


}


