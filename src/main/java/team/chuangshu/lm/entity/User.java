package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data

public class User {



    /**
     * 用户id
     */
    
    @Id
    @Column(name = "user_id")
                private String userId;

    /**
     * 登录密码
     */
    @Column(name = "password")
        private String password;

    /**
     * 角色名称
     */
    @Column(name = "role")
        private String role;


}


