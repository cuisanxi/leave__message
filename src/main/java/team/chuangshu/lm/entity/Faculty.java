package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Faculty {

    /**
     * 学院表主键，自增
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;


    /**
     * 学院负责人（副书记）用户id
     */
    @Column(name = "user_id")
        private String userId;

    /**
     * 联系方式
     */
    @Column(name = "phone")
        private String phone;


    /**
     * 姓名
     */
    @Column(name = "name")
        private String name;

    /**
     * 学院姓名
     */
    @Column(name = "faculty_name")
        private String facultyName;

}


