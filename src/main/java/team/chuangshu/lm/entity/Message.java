package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Message {



    /**
     * 假条id，8位数字与字母混合
     */
    
    @Id
    @Column(name = "message_id")
                private String messageId;

    /**
     * 学生用户id
     */
    @Column(name = "student_id")
        private String studentId;

    /**
     * 学生名字
     */
    @Column(name = "student_name")
        private String studentName;

    /**
     * 假条开始时间
     */
    @Column(name = "begin_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        private Date beginTime;

    /**
     * 假条结束时间
     */
    @Column(name = "end_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        private Date endTime;

    /**
     * 联系方式
     */
    @Column(name = "phone")
        private String phone;

    /**
     * 负责老师用户id
     */
    @Column(name = "teacher_id")
        private String teacherId;

    /**
     * 申请资料存放路径
     */
    @Column(name = "file_url")
        private String fileUrl;

    /**
     * 请假理由
     */
    @Column(name = "reason")
        private String reason;

    /**
     * 请假节数
     */
    @Column(name = "clazz_num")
        private String clazzNum;

    /**
     * 申请时间
     */
    @Column(name = "creat_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        private Date creatTime;

    /**
     * 长假标识，0标识短假，1标识长假
     */
    @Column(name = "is_longtime")
        private Integer isLongtime;

    /**
     * 一级审查标识，默认0，同意1，拒绝2
     */
    @Column(name = "level_one_sign")
        private Integer levelOneSign;

    /**
     * 一级审查意见
     */
    @Column(name = "level_one_comment")
        private String levelOneComment;

    /**
     * 一级审查意见时间
     */
    @Column(name = "level_one_time")
    //@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        private Date levelOneTime;

    /**
     * 二级审查标识，默认0，同意1，拒绝2
     */
    @Column(name = "level_two_sign")
        private Integer levelTwoSign;

    /**
     * 二级审查意见
     */
    @Column(name = "level_two_comment")
        private String levelTwoComment;

    /**
     * 二级审查意见时间
     */
    @Column(name = "level_two_time")
    //@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        private Date levelTwoTime;

    /**
     * 状态码，默认0，表示请假，1，表示申请通过，2表示申请被拒绝，3，待销假，4，已申请销假，5，同意销假
     */
    @Column(name = "status")
        private String status;

    /**
     * 销假标识，默认0，1，申请销假
     */
    @Column(name = "is_reportback")
        private Integer isReportback;

    /**
     * 学生在销假时提交的给负责人的内容
     */
    @Column(name = "reportback_submit_comment")
        private String reportbackSubmitComment;


}


