package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class TeacherClassConnect {



    /**
     * 老师的user_id
     */
    
    @Id
    @Column(name = "teacher_id")
                private Integer teacherId;

    /**
     * 班级id，班级id不能为0
     */
    @Column(name = "clazz_id")
        private Integer clazzId;


}


