package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Url {



    /**
     * url id
     */
    
    @Id
    @Column(name = "url_id")
                private Integer urlId;

    /**
     * url 路径
     */
    @Column(name = "url")
        private String url;


}


