package team.chuangshu.lm.entity;




import javax.persistence.*;
import lombok.Data;


/**
 * <p>
 * 
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Data
public class Teacher {



    /**
     * 老师用户id
     */
    @Column(name = "user_id")
        private String userId;

    /**
     * 联系方式
     */
    @Column(name = "phone")
        private String phone;

    /**
     * 姓名
     */
    @Column(name = "name")
        private String name;

    /**
     * 老师表主键，自增
     */
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
                private Integer id;


}


