package team.chuangshu.lm.mapper;

import team.chuangshu.lm.entity.Message;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */

public interface MessageMapper extends Mapper<Message> {

    public List<Message> teacherGetMessage(String teacherId);

    public List<Message> facultyGetMessage(String facultyUserId);

}