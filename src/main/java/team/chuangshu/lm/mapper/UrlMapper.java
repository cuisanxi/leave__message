package team.chuangshu.lm.mapper;

import team.chuangshu.lm.entity.Url;
import tk.mybatis.mapper.common.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
public interface UrlMapper extends Mapper<Url> {

}