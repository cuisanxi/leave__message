package team.chuangshu.lm.mapper;

import team.chuangshu.lm.entity.Teacher;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
public interface TeacherMapper extends Mapper<Teacher> {

    public List<Teacher> selectTeacherForStudent(String studentUserId);

}