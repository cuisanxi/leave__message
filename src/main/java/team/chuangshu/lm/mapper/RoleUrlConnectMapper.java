package team.chuangshu.lm.mapper;

import team.chuangshu.lm.entity.RoleUrlConnect;
import tk.mybatis.mapper.common.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */

public interface RoleUrlConnectMapper extends Mapper<RoleUrlConnect> {

}