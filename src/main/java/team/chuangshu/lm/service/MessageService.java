package team.chuangshu.lm.service;


import team.chuangshu.lm.base.service.BaseService;
import team.chuangshu.lm.entity.Message;



import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
public interface MessageService extends BaseService<Message> {


    public List<Message> teacherGetMessage(String teacherId);

    public List<Message> facultyGetMessage(String facultyUserId);

}
