package team.chuangshu.lm.service;


import team.chuangshu.lm.base.service.BaseService;
import team.chuangshu.lm.entity.Teacher;



import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */

public interface TeacherService extends BaseService<Teacher> {

    public List<Teacher> selectTeacherForStudent (String studentUserId);
}
