package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.RoleUrlConnect;
import team.chuangshu.lm.mapper.RoleUrlConnectMapper;
import team.chuangshu.lm.service.RoleUrlConnectService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class RoleUrlConnectServiceImpl extends BaseServiceImpl<RoleUrlConnectMapper, RoleUrlConnect> implements RoleUrlConnectService {


    @Autowired
    private RoleUrlConnectMapper roleUrlConnectMapper;


}