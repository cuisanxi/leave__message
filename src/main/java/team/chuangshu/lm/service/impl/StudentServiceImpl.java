package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Student;
import team.chuangshu.lm.mapper.StudentMapper;
import team.chuangshu.lm.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class StudentServiceImpl extends BaseServiceImpl<StudentMapper, Student> implements StudentService {


    @Autowired
    private StudentMapper studentMapper;


}