package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Faculty;
import team.chuangshu.lm.mapper.FacultyMapper;
import team.chuangshu.lm.service.FacultyService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class FacultyServiceImpl extends BaseServiceImpl<FacultyMapper, Faculty> implements FacultyService {


    @Autowired
    private FacultyMapper facultyMapper;


}