package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.TeacherClassConnect;
import team.chuangshu.lm.mapper.TeacherClassConnectMapper;
import team.chuangshu.lm.service.TeacherClassConnectService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class TeacherClassConnectServiceImpl extends BaseServiceImpl<TeacherClassConnectMapper, TeacherClassConnect> implements TeacherClassConnectService {


    @Autowired
    private TeacherClassConnectMapper teacherClassConnectMapper;


}