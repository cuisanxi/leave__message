package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Teacher;
import team.chuangshu.lm.mapper.TeacherMapper;
import team.chuangshu.lm.service.TeacherService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class TeacherServiceImpl extends BaseServiceImpl<TeacherMapper, Teacher> implements TeacherService {


    @Autowired
    private TeacherMapper teacherMapper;


    @Override
    public List<Teacher> selectTeacherForStudent(String studentUserId) {
        return teacherMapper.selectTeacherForStudent(studentUserId);
    }
}