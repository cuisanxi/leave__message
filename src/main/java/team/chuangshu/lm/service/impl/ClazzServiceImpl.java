package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Clazz;
import team.chuangshu.lm.mapper.ClazzMapper;
import team.chuangshu.lm.service.ClazzService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class ClazzServiceImpl extends BaseServiceImpl<ClazzMapper, Clazz> implements ClazzService {


    @Autowired
    private ClazzMapper clazzMapper;


}