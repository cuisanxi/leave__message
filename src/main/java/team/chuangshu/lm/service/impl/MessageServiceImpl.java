package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Message;
import team.chuangshu.lm.mapper.MessageMapper;
import team.chuangshu.lm.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class MessageServiceImpl extends BaseServiceImpl<MessageMapper, Message> implements MessageService {


    @Autowired
    private MessageMapper messageMapper;



    @Override
    public List<Message> teacherGetMessage(String teacherId) {
        return messageMapper.teacherGetMessage(teacherId);
    }

    @Override
    public List<Message> facultyGetMessage(String facultyUserId) {
        return messageMapper.facultyGetMessage(facultyUserId);
    }
}