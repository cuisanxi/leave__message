package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.User;
import team.chuangshu.lm.mapper.UserMapper;
import team.chuangshu.lm.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {


    @Autowired
    private UserMapper userMapper;



}