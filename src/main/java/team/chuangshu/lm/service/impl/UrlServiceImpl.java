package team.chuangshu.lm.service.impl;

import team.chuangshu.lm.base.service.impl.BaseServiceImpl;
import team.chuangshu.lm.entity.Url;
import team.chuangshu.lm.mapper.UrlMapper;
import team.chuangshu.lm.service.UrlService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Zhang
 * @since 2019-04-16
 */
@Service
public class UrlServiceImpl extends BaseServiceImpl<UrlMapper, Url> implements UrlService {


    @Autowired
    private UrlMapper urlMapper;


}