package team.chuangshu.lm.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.dto.UserDTO;
import team.chuangshu.lm.entity.*;
import team.chuangshu.lm.service.*;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    public UserService userService;

    @Autowired
    public TeacherService teacherService;

    @Autowired
    public ClazzService clazzService;

    @Autowired
    public StudentService studentService;

    @Autowired
    public FacultyService facultyService;

    @PostMapping("/insertUser")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(User user) throws Exception {
        userService.save(user);
        return ResultUtil.Success(user);
    }

    @PutMapping("/updateUserById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(User user) throws Exception {
        userService.update(user);
        return ResultUtil.Success(userService.get(user.getUserId()));
    }

    @DeleteMapping("/deleteUserById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        User oldUser = userService.get(id);
        userService.deleteById((long) id);
        return ResultUtil.Success(oldUser);
    }

    @GetMapping("/getUserPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<User> selectEntityPage(User user, int pageNum, int pageSize)throws Exception{
        return userService.findPageData(user, pageNum, pageSize);
    }

    @GetMapping("/getUserList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(User user)throws Exception{
        return ResultUtil.Success(userService.findByParams(user));
    }

    @GetMapping("/getUserDTO")
    @ApiOperation("根据userId查询个人信息")
    public  ResultDTO getUserDTO (String userId) throws Exception {
        User user = userService.get(userId);
        if (user == null) {
            return ResultUtil.Error("500","该用户不存在");
        }
        log.info(user.toString());
        String role = user.getRole();
        UserDTO userDTO = new UserDTO();
        userDTO.setRole(role);
        userDTO.setUserId(userId);
        if (role.equals("student")){

            //增加学生姓名
            Student student = new Student();
            student.setUserId(userId);
            List<Student> studentList = studentService.findByParams(student);
            userDTO.setUserName(studentList.get(0).getName());
            log.info("student",student);
            //增加班级名字
            Clazz clazz = new Clazz();
            clazz.setId(student.getClazzId());
            List<Clazz> clazzList = clazzService.findByParams(clazz);
            userDTO.setClazzName(clazzList.get(0).getClazzName());
            //增加学院名字
            Faculty faculty = new Faculty();
            faculty.setId(clazzList.get(0).getParentId());
            List<Faculty> facultyList = facultyService.findByParams(faculty);
            userDTO.setFacultyName(facultyList.get(0).getFacultyName());
            return ResultUtil.Success(userDTO);
        }else if(role.equals("teacher")){
            Teacher teacher = new Teacher();
            teacher.setUserId(userId);
            List<Teacher> teacherList = teacherService.findByParams(teacher);
            userDTO.setUserName(teacherList.get(0).getUserId());
            return ResultUtil.Success(userDTO);
        }else if (role.equals("faculty_teacher")){
            Faculty faculty = new Faculty();
            faculty.setUserId(userId);
            List<Faculty> facultyList = facultyService.findByParams(faculty);
            userDTO.setUserName(facultyList.get(0).getName());
        }
        return ResultUtil.Success(userDTO);

    }


}
