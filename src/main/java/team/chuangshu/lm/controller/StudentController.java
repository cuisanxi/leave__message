package team.chuangshu.lm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.service.StudentService;
import team.chuangshu.lm.entity.Student;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    public StudentService studentService;

    @PostMapping("/insertStudent")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(Student student) throws Exception {
        studentService.save(student);
        return ResultUtil.Success(student);
    }

    @PutMapping("/updateStudentById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(Student student) throws Exception {
        studentService.update(student);
        return ResultUtil.Success(studentService.get(student.getId()));
    }

    @DeleteMapping("/deleteStudentById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        Student oldStudent = studentService.get(id);
        studentService.deleteById((long) id);
        return ResultUtil.Success(oldStudent);
    }

    @GetMapping("/getStudentPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<Student> selectEntityPage(Student student, int pageNum, int pageSize)throws Exception{
        return studentService.findPageData(student, pageNum, pageSize);
    }

    @GetMapping("/getStudentList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(Student student)throws Exception{
        return ResultUtil.Success(studentService.findByParams(student));
    }


}
