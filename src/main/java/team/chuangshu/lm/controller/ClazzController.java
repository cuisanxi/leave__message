package team.chuangshu.lm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.service.ClazzService;
import team.chuangshu.lm.entity.Clazz;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/clazz")
public class ClazzController {

    @Autowired
    public ClazzService clazzService;

    @PostMapping("/insertClazz")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(Clazz clazz) throws Exception {
        clazzService.save(clazz);
        return ResultUtil.Success(clazz);
    }

    @PutMapping("/updateClazz}ById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(Clazz clazz) throws Exception {
        clazzService.update(clazz);
        return ResultUtil.Success(clazzService.get(clazz.getId()));
    }

    @DeleteMapping("/deleteClazzById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        Clazz oldClazz = clazzService.get(id);
        clazzService.deleteById((long) id);
        return ResultUtil.Success(oldClazz);
    }

    @GetMapping("/getClazzPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<Clazz> selectEntityPage(Clazz clazz, int pageNum, int pageSize)throws Exception{
        return clazzService.findPageData(clazz, pageNum, pageSize);
    }

    @GetMapping("/getClazzList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(Clazz clazz)throws Exception{
        return ResultUtil.Success(clazzService.findByParams(clazz));
    }


}
