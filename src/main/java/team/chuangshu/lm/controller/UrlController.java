package team.chuangshu.lm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.service.UrlService;
import team.chuangshu.lm.entity.Url;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/url")
public class UrlController {

    @Autowired
    public UrlService urlService;

    @PostMapping("/insertUrl")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(Url url) throws Exception {
        urlService.save(url);
        return ResultUtil.Success(url);
    }

    @PutMapping("/updateUrlById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(Url url) throws Exception {
        urlService.update(url);
        return ResultUtil.Success(urlService.get(url.getUrlId()));
    }

    @DeleteMapping("/deleteUrlById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        Url oldUrl = urlService.get(id);
        urlService.deleteById((long) id);
        return ResultUtil.Success(oldUrl);
    }

    @GetMapping("/getUrlPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<Url> selectEntityPage(Url url, int pageNum, int pageSize)throws Exception{
        return urlService.findPageData(url, pageNum, pageSize);
    }

    @GetMapping("/getUrlList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(Url url)throws Exception{
        return ResultUtil.Success(urlService.findByParams(url));
    }


}
