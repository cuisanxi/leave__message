package team.chuangshu.lm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.service.FacultyService;
import team.chuangshu.lm.entity.Faculty;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/faculty")
public class FacultyController {

    @Autowired
    public FacultyService facultyService;

    @PostMapping("/insertFaculty")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(Faculty faculty) throws Exception {
        facultyService.save(faculty);
        return ResultUtil.Success(faculty);
    }

    @PutMapping("/updateFacultyById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(Faculty faculty) throws Exception {
        facultyService.update(faculty);
        return ResultUtil.Success(facultyService.get(faculty.getId()));
    }

    @DeleteMapping("/deleteFacultyById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        Faculty oldFaculty = facultyService.get(id);
        facultyService.deleteById((long) id);
        return ResultUtil.Success(oldFaculty);
    }

    @GetMapping("/getFacultyPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<Faculty> selectEntityPage(Faculty faculty, int pageNum, int pageSize)throws Exception{
        return facultyService.findPageData(faculty, pageNum, pageSize);
    }

    @GetMapping("/getFacultyList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(Faculty faculty)throws Exception{
        return ResultUtil.Success(facultyService.findByParams(faculty));
    }


}
