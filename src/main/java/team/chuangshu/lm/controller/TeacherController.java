package team.chuangshu.lm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import team.chuangshu.lm.base.ResultUtil;
import team.chuangshu.lm.base.dto.ResultDTO;
import team.chuangshu.lm.base.page.PageList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import team.chuangshu.lm.service.TeacherService;
import team.chuangshu.lm.entity.Teacher;

import java.util.List;


/**
 *
 * @author Zhang
 * @since 2019-04-16
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    public TeacherService teacherService;

    @PostMapping("/insertTeacher")
    @ApiOperation(value = "插入一条数据")
    //@ApiImplicitParams()
    public ResultDTO insertOne(Teacher teacher) throws Exception {
        teacherService.save(teacher);
        return ResultUtil.Success(teacher);
    }

    @PutMapping("/updateTeacherById")
    @ApiOperation(value = "按id修改数据")
    //@ApiImplicitParams()
    public ResultDTO updateById(Teacher teacher) throws Exception {
        teacherService.update(teacher);
        return ResultUtil.Success(teacherService.get(teacher.getId()));
    }

    @DeleteMapping("/deleteTeacherById")
    @ApiOperation("按id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", dataType = "Long", required = true, value = "实体类id"),

    })
    public ResultDTO deleteById(@RequestParam(required = true) Long id) throws Exception {
        Teacher oldTeacher = teacherService.get(id);
        teacherService.deleteById((long) id);
        return ResultUtil.Success(oldTeacher);
    }

    @GetMapping("/getTeacherPages")
    @ApiOperation("按条件查询，分页")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query" , name = "pageNum" , dataType = "Integer" , defaultValue = "1" , required = true, value = "页码"),
            @ApiImplicitParam(paramType = "query" , name = "pageSize" , dataType = "Integer" , defaultValue = "10" , required = false, value = "每页容量")
    })
    public PageList<Teacher> selectEntityPage(Teacher teacher, int pageNum, int pageSize)throws Exception{
        return teacherService.findPageData(teacher, pageNum, pageSize);
    }

    @GetMapping("/getTeacherList")
    @ApiOperation("按条件查询，不分页")
    //@ApiImplicitParams()
    public ResultDTO selectEntityPage(Teacher teacher)throws Exception{
        return ResultUtil.Success(teacherService.findByParams(teacher));
    }

    @GetMapping("/selectTeacherForStudent")
    @ApiOperation("按学生学号查询负责老师")
    @ApiImplicitParam(name = "studentUserId",value = "学生学号",required = true,dataType = "String")
    public ResultDTO selectTeacherForStudent(String studentUserId)throws Exception{
        return ResultUtil.Success(teacherService.selectTeacherForStudent(studentUserId));
    }
}
