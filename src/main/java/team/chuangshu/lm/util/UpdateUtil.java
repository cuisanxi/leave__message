package team.chuangshu.lm.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import team.chuangshu.lm.entity.Message;
import team.chuangshu.lm.mapper.MessageMapper;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class UpdateUtil {

    @Autowired
    private MessageMapper messageMapper;

    @Scheduled(cron = "0 */1 * * * ?")
    public void updateMessageStatus(){
        log.info("更新执行");
        Example example = new Example(Message.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("status","1");
        criteria.andEqualTo("isReportback",1);
        criteria.andLessThan("endTime",new Date());
        List<Message> messages = messageMapper.selectByExample(example);
        log.info(messages.toString());
        for (Message message : messages) {
            message.setStatus("3");
            messageMapper.updateByExample(message,example);
        }
    }

}
